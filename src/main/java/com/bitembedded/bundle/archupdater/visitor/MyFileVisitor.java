package com.bitembedded.bundle.archupdater.visitor;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maartenc on 8/14/14.
 */
public class MyFileVisitor extends SimpleFileVisitor<Path> {

    private final List<Path> zipfiles = new ArrayList<>();

    private final PathMatcher matcher;

    private Path file;

    public MyFileVisitor(final PathMatcher matcher) {
        this.matcher = matcher;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        final Path name = file.getFileName();
        if (name != null && matcher.matches(name)) {
            zipfiles.clear();
            this.file = file;
            return FileVisitResult.TERMINATE;
        } else if (isZip(name.toString())) {
            zipfiles.add(file);
        }
        return FileVisitResult.CONTINUE;
    }

    public Path getFile() {
        return file;
    }

    public List<Path> getZipfiles() {
        return zipfiles;
    }

    private boolean isZip(final String filename) {

        final String ext = fileExtension(filename);
        if ("zip".equalsIgnoreCase(ext)) {
            return true;
        }

        if ("jar".equalsIgnoreCase(ext)) {
            return true;
        }

        return false;
    }

    private String fileExtension(final String name) {

        final int idx = name.lastIndexOf('.');
        if (idx == -1) {
            return null;
        }

        return name.substring(idx + 1);
    }
}
